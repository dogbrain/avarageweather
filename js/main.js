
/**
GEOLOCATION WITHOUT googles js
**/
function getLocationNoG()
{
	if (navigator.geolocation)
		navigator.geolocation.getCurrentPosition(showPositionNoG,showErrorNoG);
  	else
  		$("#geolocationNoG").html("Geolocation is not supported by this browser.");

}
function showPositionNoG(position)
{
	$"(#geolocationNoG").html("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);

  	var latlon = position.coords.latitude + "," + position.coords.longitude;
  	var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=400x300&sensor=false";

  	$("#mapHolderNoG").html("<img src='"+img_url+"'>");
}
function showErrorNoG(error)
{
	switch(error.code) 
    {
    	case error.PERMISSION_DENIED:
      		$("#geolocationNoG").html("User denied the request for Geolocation.");
      	break;
    	case error.POSITION_UNAVAILABLE:
      		$("#geolocationNoG").html("Location information is unavailable.");
      	break;
    	case error.TIMEOUT:
      		$("#geolocationNoG").html("The request to get user location timed out.");
      	break;
    	case error.UNKNOWN_ERROR:
      		$("#geolocationNoG").html("An unknown error occurred.");
      	break;
    }
  }

/**
 GEOLOCATION WITH google js
**/
var map;
function getLocation(){
	var mapOptions = {zoom: 6 };
  	map = new google.maps.Map(document.getElementById('mapHolder'), mapOptions);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

      $("#geolocation").html("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);

      var infowindow = new google.maps.InfoWindow({
        map: map,
        position: pos,
        content: 'Location found using HTML5.'
      });

      map.setCenter(pos);
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}


$(function() {
    $("#geolocationNoGButton").click(function(){getLocationNoG();});
    $("#geolocationButton").click(function(){getLocation();});
});