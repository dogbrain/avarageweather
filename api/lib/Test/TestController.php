<?php
namespace Test;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TestController
{
    public function getAllAction(Application $app)
    {
        //TEST WITH STATIC ARRAY
        /*$arr = array(
        	"person1"=>array("fistname"=>"Kalle", "lastname"=>"Karlsson", "Telephone"=>1111),
        	"person2"=>array("fistname"=>"Lotta", "lastname"=>"Larsson", "Telephone"=>2121),
        	"person3"=>array("fistname"=>"Johan", "lastname"=>"Johansson", "Telephone"=>111221),

        	);
        return new JsonResponse($arr);
        */

        //TEST WITH FILE_GET_CONTENTS
        /* $response = file_get_contents('http://api.openweathermap.org/data/2.5/forecast?q=Malm%C3%B6&mode=json');
        $response = json_decode($response); //just to demo json decode
        return new JsonResponse($response);
        */

        //TEST WITH CURL ASYNCH REQUESTS
        $urls = array(
            'http://api.openweathermap.org/data/2.5/forecast?q=Malm%C3%B6&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Copenhagen&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Trelleborg&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Ystad&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Lund&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Stockholm&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=G%C3%B6teborg&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Lomma&mode=json',
            'http://api.openweathermap.org/data/2.5/forecast?q=Smyge&mode=json',
            );
        $time_start = $this->microtime_float();
        $this->rolling_curl($urls, array($this, 'myCallBack'), 2, 2000);
        $time_end = $this->microtime_float();
        $response = array('start' => $time_start, 'end'=>$time_end, 'diff'=>($time_end-$time_start), 'total urls'=>sizeof($urls), 'total returned answers' =>$this->numberOfCompletedRequests);
        return new JsonResponse($response);
    }

    private $numberOfCompletedRequests=0;

    private function myCallBack($url, $output){
        $this->numberOfCompletedRequests++;
        //echo $url;
    }

    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }
  

    //http://www.onlineaspect.com/2009/01/26/how-to-use-curl_multi-without-blocking/
    private function rolling_curl($urls, $callback, $number_of_parallel_requests, $timeout_ms_per_url) {

        // make sure the rolling window isn't greater than the # of urls
        $rolling_window = (sizeof($urls) < $number_of_parallel_requests) ? sizeof($urls) : $number_of_parallel_requests;

        $master = curl_multi_init();
        $curl_arr = array();

        // add additional curl options here
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_TIMEOUT_MS => $timeout_ms_per_url);
        
        // start the first batch of requests
        for ($i = 0; $i < $rolling_window; $i++) {
            $ch = curl_init();
            $options[CURLOPT_URL] = $urls[$i];
            curl_setopt_array($ch,$options);
            curl_multi_add_handle($master, $ch);
        }

        do {
            while(($execrun = curl_multi_exec($master, $running)) == CURLM_CALL_MULTI_PERFORM);
            if($execrun != CURLM_OK)
                break;
            // a request was just completed -- find out which one
            while($done = curl_multi_info_read($master)) {
                $info = curl_getinfo($done['handle']);
                if ($info['http_code'] == 200)  {
                    $output = curl_multi_getcontent($done['handle']);

                    // request successful.  process output using the callback function.
                    $callback(curl_getinfo($done['handle'], CURLINFO_EFFECTIVE_URL), $output);

                    // start a new request (it's important to do this before removing the old one)
                    if($i<sizeof($urls)){
                        $ch = curl_init();
                        $options[CURLOPT_URL] = $urls[$i++];  // increment i
                        curl_setopt_array($ch,$options);
                        curl_multi_add_handle($master, $ch);
                    }
                    // remove the curl handle that just completed
                    curl_multi_remove_handle($master, $done['handle']);
                } else {
                    // request failed.  add error handling.
                    curl_multi_remove_handle($master, $done['handle']);
                }
            }
        } while ($running);
    
        curl_multi_close($master);
    return true;
}

/*
 	public function getOneAction($id, Application $app)
    {
        return new JsonResponse($app['db']
            ->fetchAssoc("SELECT * FROM messages WHERE id=:ID", ['ID' => $id]));
    }

    public function deleteOneAction($id, Application $app)
    {
        return $app['db']->delete('messages', ['ID' => $id]);
    }

    public function addOneAction(Application $app, Request $request)
    {
        $payload = json_decode($request->getContent());;

        $newResource = [
            'id'      => (integer)$app['db']
                ->fetchColumn("SELECT max(id) FROM messages") + 1,
            'author'  => $payload->author,
            'message' => $payload->message,
        ];
        $app['db']->insert('messages', $newResource);

        return new JsonResponse($newResource);
    }

    public function editOneAction($id, Application $app, Request $request)
    {
        $payload = json_decode($request->getContent());;
        $resource = [
            'author'  => $payload->author,
            'message' => $payload->message,
        ];
        $app['db']->update('messages', $resource, ['id' => $id]);

        return new JsonResponse($resource);
    }
*/
}