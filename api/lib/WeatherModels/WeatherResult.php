<?php
namespace WeatherModels;

class WeatherResult
{
	public $longitute;
	public $latitude;
	public $city;
	public $weather=array(); //array of WeatherData
	public $providers=array(); //array of the providers that contributed with data

}