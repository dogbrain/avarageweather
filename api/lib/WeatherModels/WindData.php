<?php
namespace WeatherModels;

class WindData{
	public $minWind;
	public $minWindProvider;
	public $avarageWind;
	public $medianWind;
	public $medianWindProvider;
	public $maxWind;
	public $maxWindProvider;
	private $totalWind;
	private $noWinds;
	private $windspeed=array();

	//http://climate.umn.edu/snow_fence/components/winddirectionanddegreeswithouttable3.htm
	public $minWindDirection;
	public $minWindDirectionProvider;
	public $avarageWindDirection;
	public $medianWindDirection;
	public $medianWindDirectionProvider;
	public $maxWindDirection;
	public $maxWindProviderDirection;
	private $totalWindDir;
	private $noWindsDir;
	private $windDirs=array();




	public function __constr($windspeed,$direction, $provider){
			$this->minWind=$windspeed;
			$this->minWindProvider=$provider;
			$this->maxWind=$windspeed;
			$this->maxWindProvider=$provider;
	
		$this->totalWind=$windspeed;
		$this->noWinds=1;
		$this->avarageWind=$windspeed;
		
		$this->windspeed[]=array('wind'=>$windspeed, 'prov'=>$provider);
		$med= (int)(sizeof($this->windspeed)/2);
		$this->medianWind=$windspeed[$med]['wind'];
		$this->medianWindProvider=$windspeed[$med]['prov'];

			$this->minWindDirection=$direction;
			$this->minWindProvider=$provider;
			$this->maxWindDirection=$direction;
			$this->maxWindProvider=$provider;
		
		$this->totalWindDir=$direction;
		$this->noWindsDir=1;
		$this->avarageWindDirection=$direction;

		$this->windDirs[]=array('wind'=>$direction, 'prov'=>$provider);
		$med= (int)(sizeof($this->windDirs)/2);
		$this->medianWindDirection=$windDirs[$med]['wind'];
		$this->medianWindDirectionProvider=$windDirs[$med]['prov'];

	}
	

	public function addWindSpeeed($windspeed,$direction, $provider){
		if($windspeed<$this->minWind){
			$this->minWind=$windspeed;
			$this->minWindProvider=$provider;
		}
		if($windspeed>$this->maxWind){
			$this->maxWind=$windspeed;
			$this->maxWindProvider=$provider;
		}

		$this->totalWind+=$windspeed;
		$this->noWinds++;
		$this->avarageWind=$this->totalWind/$this->noWinds;
		
		$this->windspeed[]=array('wind'=>$windspeed, 'prov'=>$provider);
		$med= (int)(sizeof($this->windspeed)/2);
		$this->medianWind=$windspeed[$med]['wind'];
		$this->medianWindProvider=$windspeed[$med]['prov'];

		if($direction<$this->minWindDirection){
			$this->minWindDirection=$direction;
			$this->minWindProvider=$provider;
		}

		if($direction>$this->maxWindDirection){
			$this->maxWindDirection=$direction;
			$this->maxWindProvider=$provider;
		}

		$this->totalWindDir+=$direction;
		$this->noWindsDir++;
		$this->avarageWindDirection=$this->totalWindDir/$this->noWindsDir;

		$this->windDirs[]=array('wind'=>$direction, 'prov'=>$provider);
		$med= (int)(sizeof($this->windDirs)/2);
		$this->medianWindDirection=$windDirs[$med]['wind'];
		$this->medianWindDirectionProvider=$windDirs[$med]['prov'];

	}
}

