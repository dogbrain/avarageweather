<?php
namespace Weather;

use Silex\Application;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class WeatherController
{
    private $application;

    private $errorMessage;

    public function getWeatherAction(Request $request, Application $app)
    {
        $this->application=$app;
    	$params = $request->query->all();

        //no parameters.. then display info
    	if(empty($params))
            return $this->displayInfo();
        //validate pos
        if(!$this->posIsValid($params)){
            return $this->displayError($this->errorMessage);
        }
    	$position=explode(',', $params['pos']);

        //get data from yr
    	$yrResponse = $this->getYrData($position);
        if($yrResponse==false)
            return $this->displayError("Error getting data from YR");

        $OWMResponse=$this->getOWMData($position);

        return new JsonResponse($yrResponse);
    }

    private function getOWMData($position){
        $response = @file_get_contents('http://api.openweathermap.org/data/2.5/forecast?lat='.$position[0].'&lon='.$position[1].'&units=metric');
        //echo "<pre>";
        $tempResults=array();
        foreach (json_decode($response)->{'list'} as $dataRow){
            //var_dump($dataRow);
            $tempResults[]=array(
                        'time'=>$dataRow->{'dt_txt'},
                        'temperature'=>$dataRow->{'main'}->{'temp'},
                        'windDirection'=>$dataRow->{'wind'}->{'deg'},
                        'windSpeed'=>((float)$dataRow->{'wind'}->{'speed'})/3.6, //km/h -> m/s ??????
                        );
        }
        //echo "</pre>";
        //exit;

        return $tempResults;
    }

    private function getYrData($position){
        $yrResponse = @file_get_contents('http://api.yr.no/weatherapi/locationforecastlts/1.1/?lat='.$position[0].';lon='.$position[1]);
        if($yrResponse!=false){
            $yrXml=new \SimpleXMLElement($yrResponse);
            $yrTempResults=array();
            $firstDone=false;
            foreach ($yrXml->product->time as $time) {
                $from=new \DateTime($time['from']);
                $to=new \DateTime($time['to']);
                //Check that it is every third hour 3 6 9 12 etc
                //get the item that has the data (eg from=to)2
                if(!$firstDone && $from==$to){

                    $from->sub(new \DateInterval('PT'.((int)$from->format('h')%3).'H'));
                    $yrTempResults[]=array(
                        'time'=>$from->format('Y-m-d H:i:s'),
                        'temperature'=>(string)$time->location->temperature['value'],
                        'windDirection'=>(string)$time->location->windDirection['deg'],
                        'windSpeed'=>(string)$time->location->windSpeed['mps'],
                        );
                    $firstDone=true;
                }else
                    if ((int)$from->format('h')%3==0 && $from==$to){
                    $yrTempResults[]=array(
                        'time'=>$from->format('Y-m-d H:i:s'),
                        'temperature'=>(string)$time->location->temperature['value'],
                        'windDirection'=>(string)$time->location->windDirection['deg'],
                        'windSpeed'=>(string)$time->location->windSpeed['mps'],
                        );
                }
            }
            return $yrTempResults;
        }else
            return false;
    }

    private function posIsValid($pos){
         if(empty($pos['pos'])){
            $this->errorMessage="You need to include the pos parameter in all requests.";
            return false;
         }
         if(substr_count($pos['pos'], ',')!=1){
            $this->errorMessage="The pos has an illegal value.";
            return false;
         }
         $position=explode(',', $pos['pos']);
        if(!Helpers::isValidLatitude($position[0])){
            $this->errorMessage="The latitude has an illegal value.";
            return false;
         }

        if(!Helpers::isValidLongitude($position[1])){
            $this->errorMessage="The longitude has an illegal value.";
            return false;
         }
         return true;
    }

   

    private function displayInfo(){

        $mess=array(
            'message'=>'Information of how to use the avarage weather', 
            'required_parameters'=> 
            array(
                'pos', 
                array('description'=>'the latitude and longitude with a separaing comma', 
                    'example'=>$this->application['request']->getUri().'?pos=55.604981,13.003822')),
            'optional_parameters'=>'none yet');
        return new JsonResponse($mess);
    }

    private function displayError($message){

    	$mess=array('message'=>$message, 'more_information'=> 'http://'.$this->application['request']->getHttpHost().'/api/v1/averageweather/weather');
    	return new JsonResponse($mess, 400);
    }
}