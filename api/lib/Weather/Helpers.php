<?php
namespace Weather;

use Silex\Application;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class Helpers
{
	 //http://altafphp.blogspot.se/2013/11/validate-latitude-and-longitude-with.html
    public static function isValidLatitude($latitude){
    	if (preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $latitude)) {
        	return true;
    	} else {
        	return false;
    	}
  	}
  	public static function isValidLongitude($longitude){
    	if(preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/",$longitude)) 
    	{
       		return true;
    	} else {
      		return false;
    	}
  	}
  }